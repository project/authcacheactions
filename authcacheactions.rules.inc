<?php
// $Id;

/**
 * @file
 * This file provides the rules integration for this module.
 */

/**
 * Implementation of hook_rules_action_info().
 */
function authcacheactions_rules_action_info() {
  $actions = array();
  
  $actions = array(
    'authcacheactions_action_block_cache_page' => array(
      'label' => t('Clear cache by path'),
      'arguments' => array(
        'path' => array(
          'type' => array('string', 'uri'), 
          'label' => t('Cache path'), 
          'description' => t('Patch to be cleared with http(s)://')
        ),        
        'table' => array(
          'type' => 'text', 
          'label' => t('Table cache'),
          'default value' => 'cache_page',
          'description' => t('Table where path cache will be cleared. Default: cache_page.'),
          'default mode' => 'input',
          'restrict' => 'input',
        ),        
      ),
      'group' => t('Cache'),
      'module' => 'authcacheactions',
    )
  );
  
  return $actions;
}

/**
 * Block cache page
 *
 * @param string $path
 * Url where cache will be cleaned
 * @param string $table 
 * Table where clean cache
 */
function authcacheactions_action_block_cache_page($path, $table = 'cache_page') {
  global $base_root;
  
  $cache_roles = (array)variable_get('authcache_roles', array());

  if (count($cache_roles) > 0) {
    $cache_roles_checked = array_keys($cache_roles, 1);
    
    if (count($cache_roles_checked) > 0) {
      asort($cache_roles);
      $cache_roles = array_keys($cache_roles);        
      
      do {
        $keys = array();
        _authcacheactions_generate_permutation(1, count($cache_roles), $cache_roles, $keys);
        array_shift($cache_roles);
        
        if (!empty($keys)) {
          foreach ($keys as $value) {
            $key = substr(md5($key . drupal_get_private_key()), 0, 6) . $path;
            //watchdog('authcacheactions', $key . ' ' . $table);
            cache_clear_all($key, $table);
          }
        }
        
        unset($keys);
      } while (!empty($cache_roles));
    }
  }
}

/**
 * Generate all permutation
 *
 * @param int $l
 * @param int $r
 * @param array $data
 * @param array $output
 */
function _authcacheactions_generate_permutation($l, $r, &$data, &$output) {
  if ($l == $r) {
    $output[] = implode('.', $data);
  }
  else {
    for ($i = $l - 1; $i < $r; $i++) {
      $v = $data[$l - 1]; $data[$l - 1] = $data[$i]; $data[$i] = $v;
      _authcacheactions_generate_permutation($l + 1, $r, $data, $output);
      $v = $data[$l - 1]; $data[$l - 1] = $data[$i]; $data[$i] = $v;
    }
  }
}