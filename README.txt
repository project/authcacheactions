-- SUMMARY --

Gives you the ability to clear Authcache cache with rules actions based by path.


-- REQUIREMENTS --

Rules http://drupal.org/project/rules
Authcache http://drupal.org/project/authcache


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.


-- HOWTO --

1. Go to admin/config/workflow/rules and create rule
2. Provide name and choose "React on event" for example "After updating existing content"
3. Add action "Clear cache by path"
4. Provide Data selector for cache path, for example node:edit-url
5. Provide Table Cache (optional)
6. Enjoy cache clear for all roles enbaled in Authcache even for multiple roles after node edit

-- FAQ --
